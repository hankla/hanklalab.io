---
title: PREVIOUS RESEARCH
description: 'Previous Research Interests'
---

![](compare_lv05-5_pres-dens-2.png)
## Thermal Torques
Luminous bodies in an accretion disk---for instance, an accreting planetesimal in a protoplanetary disk---emit radiation that heats up the gas of the disk. This heating results in asymmetric underdensities of gas and thus a net torque on the body, leading to orbital migration of the body. During my summer at the CCA, I worked with Phil Armitage and Yan-Fei Jiang to test the linear theory of this "heating torque". We found that the linear theory underpredicts the acceleration of the body in the high conductivity regime, and that the theory originally developed for modelling planets in a disk could also be applied to stars or accreting stellar mass black holes embedded in Active Galactic Nucleus disks.

This work was published in the [Astrophysical Journal in 2020](https://ui.adsabs.harvard.edu/abs/2020ApJ...902...50H/abstract).

![](butterfly.jpg)
## Non-helical Dynamo Mechanisms
Astrophysical systems such as accretion disks around black holes are able to sustain magnetic fields on timescales much longer than the resistive decay timescale. Dynamo theory describes how these magnetic fields are maintained. Although mean-field dynamo theory is well-developed in systems with a density gradient, the driving mechanisms in systems that lack such a gradient are still under debate. I explore two possible mechanisms, the magnetic shear-current effect and the stochastic-alpha effect, and their connection to dynamo transport coefficients.

This research was one of the topics during my Fulbright fellowship.
It was presented at the 2018 EPS meeting ([proceedings](proceedings_eps_2018.pdf)).
Note the typo in the second sentence (the MRI transports angular momentum outward).

![](anisotropy.png)
## MHD Closure for Kinetic Pressure Anisotropy Instabilities
Accretion disks around black holes are often simulated as magnetohydrodynamic fluids, a model which is only valid when the plasma that makes up the disk is highly collisional. However, in systems such as radiatively inefficient accretion disks, the collisional mean free path is much larger than the disk's characteristic length, meaning that a kinetic approach should be more accurate. However, the PIC and hybrid simulations needed to model kinetic effects are much more computationally expensive than simulations that solve the standard MHD equations. Can we somehow modify the MHD equations to accurately capture the relevant kinetic (firehose and mirror) instabilities?

This research was the topic of my [undergraduate thesis](hankla_undergraduate_princeton_thesis_2017.pdf).

## Other Undergraduate Work
I completed two research projects ("junior papers") during my third year at Princeton: the first under Frans Pretorius about [the cyclic model of cosmology](hankla_undergraduate_princeton_jp_2016_fall.pdf) and the second under Herman Verlinde on [radiation of a uniformly accelerating charge](hankla_undergraduate_princeton_jp_2017_spring.pdf).

Last updated: 2021-03-26

---
title: OTHER PROJECTS
description: 'Resources'
---

A collection of miscellaneous (non-research) projects from over the years.

## NASA APOD
Automatically set NASA's [Astronomy Picture of the Day](https://apod.nasa.gov/apod/astropix.html) as your desktop.
[GitLab repository](https://gitlab.com/hankla/apod_background).

## Boulder Coffee Shops
A *satirical* piece ranking coffee shops in Boulder very scientifically.
Highlights:
- Espresso quality increases with distance from JILA.
- Productivity increases slightly with distance from JILA.

Details on this work can be found in this [Google Drive folder](https://drive.google.com/drive/folders/1VapOlnGZwtGGumkLnk0bOymKKd-FrviM?usp=share_link), which includes [my raw data](https://docs.google.com/spreadsheets/d/125Cm0bzndaYoPdpV4eIR6LeHbRBUyS7ddxGArE9Aq-M/edit?usp=share_link), the [analysis scripts](https://colab.research.google.com/drive/1BmjSsiCt90MtOyJ3vVko3jlWZBy3vURi?usp=sharing), and [my write-up](https://drive.google.com/file/d/1m4pk04G9dpyJ594xxJ-qDi0hJgS1sN7f/view?usp=share_link) (detailing the very robust research methods). For the curious (or the concerned advisor), I tracked how much time I spent on this project. The total was 5 hours and 15 minutes. Enjoy!

## Public Talks
In spring 2023, I gave a talk oriented towards undergraduate physics students through the CU Prime student group.
That talk was recorded and is now [posted on YouTube](https://www.youtube.com/watch?v=ItNw8NFQda0).

Last updated: 2023-05-10

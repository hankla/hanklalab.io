---
title: ABOUT ME
description: Welcome to my website
images: ["/images/apod.jpg"]
---
Hi there! My full name is Amelia, but please call me Lia.

### Scientific Background
I am a theoretical/computational astrophysicist broadly interested in plasma astrophysics, particularly in black hole accretion disks (see [Research Interests](/research)).
I currently hold a JSI Neil Gehrels Prize Postdoc and a Multi-Messenger Plasma Physics Center fellowship at the University of Maryland, College Park.

I received my PhD in Physics in August 2023 from the University of Colorado, Boulder, after starting in August 2018 with the support of a National Science Foundation Graduate Research Fellowship.
During my PhD, I worked within the JILA astrophysics theory group.
I collaborated with Jason Dexter, Mitch Begelman, and Dmitri Uzdensky among others.
Before starting at CU, I spent a year in Heidelberg, Germany on a Fulbright Research Fellowship.
There, I worked with Christian Fendt at the Max Planck Institute for Astronomy and took a few classes at the University of Heidelberg.
I completed my bachelor's degree in physics at Princeton with a thesis under the supervision of Jim Stone and Matt Kunz (2017).

Learn more by checking out:

[Publications](https://scholar.google.com/citations?hl=en&user=fzJIYwEAAAAJ&view_op=list_works&sortby=pubdate "A list of my publications")

[Comprehensive CV](https://1drv.ms/b/s!AhxwVYs_9_vChJdKgWsolk3ooqdtPg?e=PIU0O8 "A running (overly long) CV")

### Personal Background
I grew up in Lafayette, Colorado thinking that towns had to have farms between them.
Nowadays in my copious amounts of free-time, I rotate between a subset of hobbies
I enjoy backpacking, playing oboe, making cheese, drinking espresso, and playing games: board games (particularly Pandemic), grand-strategy PC games (notably Stellaris and Civ VI), and tabletop games (Pathfinder 2e).
Check out [Other Projects](/other-projects) for more.

I am always happy to chat about my research, about science in general, about what it's like to be a physicist, having a(n apparently non-obvious) nickname, and pretty much anything else within reason.

As I figure out academia, I create notes containing useful ideas for people in similar situations.
My collection of [Resources](/resources) includes everything from lists of science summer schools for high-school students, CU-specific research opportunities for undergraduates, cheat sheets for setting up Windows Subsystem for Linux, and my experience applying to postdoctoral positions.
Check them out and let me know what you think!
These are in varying stages of being polished, so go easy on me.

You can contact me via email: lia [dot] hankla [at] gmail.com.

Last updated: 2024-01-31

---
title: RESOURCES
description: 'Resources'
---

This page hosts a collection of resources that I have accumulated over the years.
I put them on this website for easy access.
You can find all my resources in [this google drive folder](https://drive.google.com/drive/folders/17PTowXJChAFZb7z79Vuqp7ffD-cuA94J?usp=share_link), including some [technical resources](https://drive.google.com/drive/folders/1KdzI5SA82b9U5cvIurULVBOaV-Lr4X3-?usp=sharing).
Please feel free to email me at any time about any of the information contained in these documents.

## Fellowships
- My personal [list of graduate school fellowships](https://docs.google.com/document/d/18k_L_DqFQv5dWT8_XjBBucgGfeCKP0RQbIxd_HICshQ/edit?usp=sharing) (focusing on computational physics, astrophysics).
- My personal [list of postdoctoral fellowships](https://docs.google.com/document/d/1fCvVSvOYQpd9JuGrYsia0j8_afbmNA5hxX7SSacXmGs/edit?usp=sharing) (focusing on computational physics, astrophysics).

## Undergraduate/Graduate
- My personal [list of undergraduate research opportunities at CU](https://docs.google.com/document/d/1dLtEyhO3kqFuhLfMpsTw--G87jmfcwK5Nc7aiMkDoBU/edit?usp=sharing), focusing on physics/astro at CU but including some general research opportunites outside CU.
- My personal [list of conferences and summer schools](https://docs.google.com/document/d/1gJvW12tJHLJODUWa5z-xEmLRlvPi2zZVpVtS9lM_OIQ/edit?usp=sharing), including information on travel grants.


Last updated: 2022-11-09

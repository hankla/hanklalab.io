---
title: RESEARCH INTERESTS
description: 'Research Interests'
---

I use numerical and analytic models to study how plasmas behave in a wide variety of astrophysical contexts.
I have studied collisionless plasmas and the fundamental physical mechanisms in them, as well as weakly collisional plasmas where Coulomb collisions cannot be ignored.
These plasmas can be found in accretion disks around black holes as well as high-energy systems such as pulsar wind nebulae.
I am interested in bridging these plasma regimes via MHD approximations to kinetic instabilities.
I also enjoy constructing semi-analytic models that use simulation results to explain observational trends.

Below are some recent project highlights.

## Nonthermal emission from the plunging region
### What is the plunging region?
A spinning black hole has many different important surfaces.
The one most people are familiar with is the event horizon: not even light can escape from within this radius.
Another important surface is the innermost stable circular orbit (ISCO): within this radius, a massive particle cannot have a stable circular orbit.
Instead, particles start "plunging" into the black hole. Orbits change from being circular outside the ISCO to mostly radial inside the ISCO.
Everything inside the ISCO is called the plunging region.

![](black-hole-surfaces.png)

### Why is the plunging region interesting?
As you can imagine, the curvature of spacetime is closest right next to the black hole.
Plasma within the plunging region is thus strongly influenced by general relativity.
The canonical model for a thin accretion disk (Shakura & Sunyaev 1973) assumes that no light is radiated from within the ISCO.
Later models (both analytic and numerical) revise that assumption, but still generally model the plasma as a fluid.
However, because the plasma is falling into the black hole so quickly, the fluid assumption can break down and lead to a semi-collisional plasma within the ISCO.
Magnetic reconnection can accelerate particles to very high energies that don't get thermalized before falling into the black hole, leading to a hybrid electron distribution.
What would we see from this hybrid electron distribution?

### How do you study the plasma in the plunging region?
This project took a semi-analytic route, building an expression for the electron distribution at each radius off of a background model for the density and magnetic field profiles.
We used a prescription from particle-in-cell simulations of magnetic reconnection to help build the electron distribution.
Once the electron distribution is constructed, we assumed either synchrotron or inverse Compton radiation to get the photon distribution.
Then, we raytrace the photons back to a camera to account for general relativistic effects such as Doppler shifting and intensity beaming.

### What do you find?
We find that the model reproduces observations well.
The hope is eventually to use this model as a way to measure magnetic fields or black hole properties: input an observable and see what model parameters best fit it.
Currently, our model trends are washed out by observational variation, so better observations will be needed before we can quantitatively test this model.

This work was published in the Monthly Notices of the Royal Astronomical Society in 2022: [AdS Link](https://ui.adsabs.harvard.edu/abs/2022MNRAS.515..775H/abstract).


## Imbalanced Turbulence
![](ExBz-slice.png)

### What is imbalanced turbulence?
Typical magnetohydrodynamic models for turbulence in a plasma with a background magnetic field assume equal fluxes of magnetic energy propagating parallel/anti-parallel to the background magnetic field.
When the magnetic energy fluxes are not equal, usually due to asymmetric injection of energy, we call the resulting turbulence "imbalanced".
### Why is imbalanced turbulence important?
Many astrophysical objects experience asymmetric energy injection, including pulsar wind nebulae and accretion disk coronae.
If imbalanced turbulence looks different than the canonical balanced turbulence, that could have implications for observations of these astrophysical systems.
### How do you study imbalanced turbulence?
I use particle-in-cell simulations of a relativistic, collisionless plasma in a periodic simulation domain to study imbalanced turbulence.
I drive the plasma with current modes of unequal amplitude that generate Alfv\'en waves propagating parallel and anti-parallel to the background magnetic field with different amplitudes.
### What do you find?
I find that particle acceleration remains efficient even when the driving creating turbulence is strongly imbalanced, although the timescales are significantly longer in the imbalanced case.
This finding agrees with quasi-linear theories and could suppress particle acceleration in systems where the light-crossing time of the system is shorter than the acceleration time scale.

I also find that imbalanced driving can launch significant outflows, on the order of 20% the speed of light.
The speed of the outflow depends on the magnetization of the plasma.
Pending further exploration of the parameter dependence of this effect, this momentum transfer mechanism could present a new wind-driving mechanism, possibly relevant for UltraFast Outflows (UFOs) seen in some active galactic nuclei.

This work was presented at APS DPP 2020/2021 and was published in the Monthly Notices of the Royal Astronomical Society in 2022: [AdS Link](https://ui.adsabs.harvard.edu/abs/2022MNRAS.509.3826H/abstract).

![](magnetic-energy-spectrum.png)


Last updated: 2022-09-05
